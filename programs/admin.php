<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';



class LibInfobip_ConfigurationPage
{


    private function getForm()
    {
        $W = bab_Widgets();
        $form = $W->Form();
        $form->setName('configuration')->addClass('BabLoginMenuBackground')->addClass('libinfobip-form');
        $form->setHiddenValue('tg', bab_rp('tg'));
        $form->colon();

        $form->getLayout()->setVerticalSpacing(1,'em');

        $form->addItem($W->Title(LibInfobip_translate('Infobip'),2));

        $label = $W->Label(LibInfobip_translate('Disable'));
        $input = $W->CheckBox()->setAssociatedLabel($label)->setName('infobip_disable');

        $form->addItem($W->VBoxItems($label, $input));

        $label = $W->Label(LibInfobip_translate('User'));
        $input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('infobip_user');

        $form->addItem($W->VBoxItems($label, $input));

        $label = $W->Label(LibInfobip_translate('Password'));
        $input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('infobip_password');

        $form->addItem($W->VBoxItems($label, $input));

        $label = $W->Label(LibInfobip_translate('Number'));
        $input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('infobip_number');

        $form->addItem($W->VBoxItems($label, $input));

        $form->addItem(
                $W->SubmitButton()
                ->setLabel(LibInfobip_translate('Save'))
        );

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibInfobip/');


        $form->setValues(
            array(
                'configuration' => array(
                    'infobip_disable'		=> $registry->getValue('infobip_disable'),
                    'infobip_user'			=> $registry->getValue('infobip_user'),
                    'infobip_password' 		=> $registry->getValue('infobip_password'),
                    'infobip_number' 		=> $registry->getValue('infobip_number')
                )
            )
        );

        return $form;
    }




    public function display()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet($GLOBALS['babInstallPath'].'styles/addons/LibInfobip/main.css');


        $page->addItem($this->getForm());
        $page->displayHtml();
    }


    public function save($configuration)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibInfobip/');

        $registry->setKeyValue('infobip_disable'			, $configuration['infobip_disable']);
        $registry->setKeyValue('infobip_user'				, $configuration['infobip_user']);
        $registry->setKeyValue('infobip_password'			, $configuration['infobip_password']);
        $registry->setKeyValue('infobip_number'				, $configuration['infobip_number']);
    }
}


if (!bab_isUserAdministrator())
{
    return;
}


$page = new LibInfobip_ConfigurationPage;

if (!empty($_POST))
{
    $page->save(bab_pp('configuration'));
}

$page->display();